package com.babl.citybank;

import com.babl.citybank.common.EventType;
import com.babl.citybank.modules.master.repositories.AuditLogRepository;
import com.babl.citybank.modules.master.repositories.CityMasterRepository;
import com.babl.citybank.modules.master.schemas.AuditLog;
import com.babl.citybank.modules.master.schemas.CityMaster;
import com.babl.citybank.modules.master.services.AuditLogService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestTest {
    @Autowired
    private AuditLogRepository auditLogRepository;


    @Test
    public void test(){

        /*AuditLog auditLog = new AuditLog();
        auditLog.setData("কিছু না");
        auditLog.setEventTime(LocalDateTime.now());
        auditLog.setEventType(EventType.INSERT.toString());
        auditLogRepository.save(auditLog);
        System.out.println(auditLog.getData());
        System.out.println(auditLog.getId());*/

        AuditLog auditLog = new AuditLog();
        auditLog.setData("একটা কিছু");
        auditLog.setEventTime(LocalDateTime.now());
        auditLog.setEventType(EventType.INSERT.toString());

        auditLogRepository.save(auditLog);

    }
}
