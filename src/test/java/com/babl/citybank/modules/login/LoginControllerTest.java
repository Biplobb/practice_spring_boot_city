package com.babl.citybank.modules.login;

import lombok.RequiredArgsConstructor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@RequiredArgsConstructor
public class LoginControllerTest {
    @Autowired
    private final LoginController loginController;

    @Autowired
    private final MockMvc mockMvc;

    @Test
    public void login() {
        assertEquals(200, loginController.login());
    }
}