package com.babl.citybank.modules.generic.services;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;


public class GenericServiceTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void selectGeneric() {
        Assert.assertEquals(1, 1);
        Assert.assertEquals(1, 1);
        Assert.assertEquals(1, 1);
    }

    @Test
    public void selectAllQuery() {
        Assert.assertEquals(1, 1);
        Assert.assertEquals(1, 1);
        Assert.assertNotEquals(9, 2);
    }
}