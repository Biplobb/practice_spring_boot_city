package com.babl.citybank.modules.generic.services;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.Authentication;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TestServicesTest {
    @Autowired
    private TestServices testServices;

    @Test
    public void checkEquality() {
        assertEquals(true, testServices.selectSomething());
    }

    @Test
    public void securityTest(){
        Authentication authentication = testServices.security();
        System.out.println(authentication.getDetails());
        assertNotNull(authentication);
    }
}