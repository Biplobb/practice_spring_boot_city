package com.babl.citybank.modules.generic.dtos;

import lombok.Data;

@Data
public class GenericVariable {
    private String name;

    private String value;

    @Override
    public String toString() {
        return "GenericVariable{" +
                "name='" + name + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
