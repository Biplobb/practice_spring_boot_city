package com.babl.citybank.modules.generic.services;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class TestServices {
    @PersistenceContext
    private EntityManager entityManager;



    public EntityManager getEntityManager(){
        EntityManagerFactory entityManagerFactory = entityManager.getEntityManagerFactory();

        return entityManagerFactory.createEntityManager();
    }

    public boolean selectSomething(){
        EntityManager newEntityManager = getEntityManager();

        String qString = "select * from audit_log";
        Query query = newEntityManager.createNativeQuery(qString);

        List<Object[]> objectArrays = query.getResultList();
        for (Object[] objectArray : objectArrays){
            for (Object object : objectArray){
                System.out.println(object);
            }
        }
        return true;
    }

    public Authentication security(){
        List<GrantedAuthority> grantedAuthorityList = new ArrayList<>();
        Authentication auth = new UsernamePasswordAuthenticationToken("user", "Pas", grantedAuthorityList);
        ((UsernamePasswordAuthenticationToken) auth).setDetails("reza");
        return auth;
    }
}

