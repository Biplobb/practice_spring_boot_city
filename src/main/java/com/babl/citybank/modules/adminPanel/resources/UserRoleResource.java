package com.babl.citybank.modules.adminPanel.resources;

import lombok.Data;

@Data
public class UserRoleResource {
    private Integer id;

    private String username;

    private String authority;
}
