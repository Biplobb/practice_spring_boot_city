package com.babl.citybank.modules.adminPanel.schemas;


import lombok.Data;

import javax.persistence.*;

@Data
@Entity(name = "menu")
public class Menu {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private Integer lft;

    @Column(nullable = false)
    private Integer rgt;

    @Column(nullable = false)
    private Integer parentId;
}
