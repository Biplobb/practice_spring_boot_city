package com.babl.citybank.modules.adminPanel.dtos;

import lombok.Data;

@Data
public class RoleMenuAddUpdateDTO {
    private Integer roleId;

    private Integer menuId;

}