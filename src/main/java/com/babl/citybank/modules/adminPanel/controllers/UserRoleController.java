package com.babl.citybank.modules.adminPanel.controllers;


import com.babl.citybank.modules.adminPanel.resources.StatusResource;
import com.babl.citybank.modules.adminPanel.resources.UserRoleResource;
import com.babl.citybank.modules.adminPanel.services.UserRoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserRoleController {
    private final UserRoleService userRoleService;

    @RequestMapping(value = "/user/assignRole", method = RequestMethod.POST)
    public StatusResource assignRole(@RequestParam("roleId")final int roleId,
                                     @RequestParam("username") final String username){
        boolean status = userRoleService.assignUserRole(roleId, username);
        StatusResource statusResource = new StatusResource("SUCCESSFUL", "");
        if (!status)
            statusResource.setStatus("UNSUCCESSFUL");
        return statusResource;
    }

    @RequestMapping(value = "/user/unassignRole/{userRoleId}", method = RequestMethod.POST)
    public StatusResource unassignRole(@PathVariable("userRoleId") final int userRoleId){
        boolean status = userRoleService.unassignUserRole(userRoleId);
        StatusResource statusResource = new StatusResource("SUCCESSFUL", "");
        if (!status)
            statusResource.setStatus("UNSUCCESSFUL");
        return statusResource;
    }


    @RequestMapping(value = "/users/getByRoleId/{roleId}", method = RequestMethod.GET)
    public List<UserRoleResource> getUsersOfRole(@PathVariable("roleId") final int roleId){
        return userRoleService.getUsersOfARole(roleId);
    }


    @RequestMapping(value = "/user/getRole/{username}", method = RequestMethod.GET)
    public List<String> getRolesOfUser(@PathVariable("username") final String username){
        return userRoleService.getRoleOfUser(username);
    }
}
