package com.babl.citybank.modules.adminPanel.resources;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class StatusResource {
    private String status;

    private String message;
}
