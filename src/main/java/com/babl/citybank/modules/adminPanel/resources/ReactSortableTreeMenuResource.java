package com.babl.citybank.modules.adminPanel.resources;

import lombok.Data;

import java.util.List;

@Data
public class ReactSortableTreeMenuResource {
    private Integer menuId;

    private String title;

    private Integer left;

    private List<ReactSortableTreeMenuResource> children;
}
