package com.babl.citybank.modules.adminPanel.resources;


import lombok.Data;

import java.util.List;

@Data
public class MenuGridResource {
    private int id;

    private String name;

    private Integer left;

    private List<MenuGridResource> child;
}
