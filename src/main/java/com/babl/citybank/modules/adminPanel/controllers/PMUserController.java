package com.babl.citybank.modules.adminPanel.controllers;


import com.babl.citybank.modules.adminPanel.services.PMUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class PMUserController {

    private final PMUserService pmUserService;

    @RequestMapping(value = "users/get/all", method = RequestMethod.GET)
    public List<String> getUsernames(){
        return pmUserService.getUserNames();
    }
}
