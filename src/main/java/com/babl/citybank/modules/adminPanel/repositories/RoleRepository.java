package com.babl.citybank.modules.adminPanel.repositories;


import com.babl.citybank.modules.adminPanel.schemas.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Integer> {
    Role findByAuthority(String authority);
    Role findById(int id);
}
