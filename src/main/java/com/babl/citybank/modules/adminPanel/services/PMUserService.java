package com.babl.citybank.modules.adminPanel.services;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Transactional
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class PMUserService {
    public List<String> getUserNames(){
        RestTemplate restTemplate = new RestTemplate();
        PMRequest pmRequest = new PMRequest();
        List<String> usernames = new ArrayList<>();

        pmRequest.setGrant_type("password");
        pmRequest.setScope("*");
        pmRequest.setClient_id("SOSYRQZTRSXQJTWKGAHUQWSJKRPBPIUW");
        pmRequest.setClient_secret("8001173835cb6b0c88e0a77015724755");
        pmRequest.setUsername("babl");
        pmRequest.setPassword("babl360");

        Object object = restTemplate.postForObject("http://192.168.10.211:8088/workflow/oauth2/token", pmRequest, Object.class);
        Map<String, ?> response = (Map)object;
        String accessToken = (String) response.get("access_token");

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("Authorization", "Bearer " + accessToken);
        //httpHeaders.set("Authorization", "Bearer " + "6f65c4a4e97c53c61a912aaa954498d539ce82c5");
        HttpEntity<?> entity = new HttpEntity<Object>(httpHeaders);


        Object object2 = restTemplate.exchange("http://192.168.10.211:8088/api/1.0/workflow/users", HttpMethod.GET, entity, Object.class);
        ResponseEntity responseEntity = (ResponseEntity)object2;
        List<Object> users = (List)responseEntity.getBody();

        for (Object user : users){
            Map<String, String> userInfo = (Map)user;
            usernames.add(userInfo.get("usr_username"));
        }

        return usernames;
    }

}


@Data
class PMRequest{
    private String grant_type;

    private String scope;

    private String client_id;

    private String client_secret;

    private String username;

    private String password;
}