package com.babl.citybank.modules.master.services;

import com.babl.citybank.common.EventType;
import com.babl.citybank.modules.master.dtos.SectorMasterAddUpdateDTO;
import com.babl.citybank.modules.master.repositories.SectorMasterRepository;
import com.babl.citybank.modules.master.resources.SectorMasterResource;
import com.babl.citybank.modules.master.schemas.SectorMaster;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class SectorMasterService {
    private final SectorMasterRepository sectorMasterRepository;
    private final AuditLogService auditLogService;

    public boolean addSectorMaster(SectorMasterAddUpdateDTO sectorMasterAddUpdateDTO) {
        SectorMaster sectorMaster = new SectorMaster();
        sectorMaster.setSectorCode(sectorMasterAddUpdateDTO.getSectorCode());
        sectorMaster.setSectorName(sectorMasterAddUpdateDTO.getSectorName());
        sectorMaster.setStatus(sectorMasterAddUpdateDTO.getStatus());
        sectorMasterRepository.save(sectorMaster);
        log.info("SectorMaster added sucessfully");

        Map<String, Object> mapData = new HashMap<>();
        mapData.put("sector_name", sectorMaster.getSectorName());
        mapData.put("sector_code", sectorMaster.getSectorCode());
        mapData.put("status", sectorMaster.getStatus());

        auditLogService.addAuditLog(sectorMaster, "sector_master", sectorMaster.getId(), EventType.INSERT.toString(), mapData.toString());


        return true;
    }

    public boolean updateSectorMaster(SectorMasterAddUpdateDTO sectorMasterAddUpdateDTO, int id) {
        SectorMaster sectorMaster = sectorMasterRepository.getOne(id);
        if (sectorMaster == null) {
            log.info("SectorMaster not found with id = " + id);
            return false;
        }
        sectorMaster.setSectorCode(sectorMasterAddUpdateDTO.getSectorCode());
        sectorMaster.setSectorName(sectorMasterAddUpdateDTO.getSectorName());
        sectorMaster.setStatus(sectorMasterAddUpdateDTO.getStatus());
        sectorMasterRepository.save(sectorMaster);
        log.info("SectorMaster updated sucessfully with id = " + id);

        Map<String, Object> mapData = new HashMap<>();
        mapData.put("sector_name", sectorMaster.getSectorName());
        mapData.put("sector_code", sectorMaster.getSectorCode());
        mapData.put("status", sectorMaster.getStatus());

        auditLogService.addAuditLog(sectorMaster, "sector_master", sectorMaster.getId(), EventType.UPDATE.toString(), mapData.toString());


        return true;
    }

    public boolean deleteSectorMaster(int id) {
        SectorMaster sectorMaster = sectorMasterRepository.getOne(id);
        if (sectorMaster == null) {
            log.info("SectorMaster not found with id = " + id);
            return false;
        }
        sectorMasterRepository.delete(sectorMaster);
        log.info("SectorMaster deleted sucessfully with id = " + id);
        return true;
    }


    public SectorMasterResource getSectorMasterResource(int id){
        SectorMaster sectorMaster = sectorMasterRepository.getOne(id);
        if (sectorMaster == null){
            log.info("SectorMaster not found with id = " + id);
            return null;
        }
        SectorMasterResource sectorMasterResource = new SectorMasterResource();
        sectorMasterResource.setId(sectorMaster.getId());
        sectorMasterResource.setSectorCode(sectorMaster.getSectorCode());
        sectorMasterResource.setSectorName(sectorMaster.getSectorName());
        sectorMasterResource.setStatus(sectorMaster.getStatus());
        sectorMasterResource.setCreatedBy(sectorMaster.getCreatedBy());
        sectorMasterResource.setCreatedDate(sectorMaster.getCreatedDate());
        sectorMasterResource.setModifiedBy(sectorMaster.getModifiedBy());
        sectorMasterResource.setModifiedDate(sectorMaster.getModifiedDate());
        log.info("SectorMaster returned sucessfully with id = " + id);
        return sectorMasterResource;
    }

    public List<SectorMaster> getAllSectorMaster() {
        return sectorMasterRepository.findAll();
    }

}