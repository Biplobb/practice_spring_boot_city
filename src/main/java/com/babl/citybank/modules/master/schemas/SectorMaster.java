package com.babl.citybank.modules.master.schemas;

import com.babl.citybank.common.Status;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
public class SectorMaster extends BaseAuditingEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "sector_code")
    private String sectorCode;

    @Column(name = "sector_name")
    private String sectorName;

    @Column(name = "status", columnDefinition = "ENUM('ACTIVE', 'INACTIVE')")
    @Enumerated(EnumType.STRING)
    private Status status;
}
