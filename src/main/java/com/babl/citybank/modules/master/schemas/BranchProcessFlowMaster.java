package com.babl.citybank.modules.master.schemas;


import com.babl.citybank.common.BranchGlobalGroupName;
import com.babl.citybank.common.SDRegion;
import com.babl.citybank.common.Status;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@Entity
public class BranchProcessFlowMaster extends BaseAuditingEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY )
    private int id;
    private BranchGlobalGroupName globalGroupName;
    private String userId;
    private String localGroupId;
    private SDRegion region;
    @ManyToOne
    @JoinColumn(name = "branch_id", referencedColumnName = "id")
    private BranchMaster branchMaster;

    @Column(name = "status", columnDefinition = "ENUM('ACTIVE', 'INACTIVE')")
    @Enumerated(EnumType.STRING)
    private Status status;
}
