package com.babl.citybank.modules.master.controllers;

import com.babl.citybank.common.StatusResource;
import com.babl.citybank.modules.master.dtos.SdProcessFlowMasterDTO;
import com.babl.citybank.modules.master.resources.SdDepartmentMasterResource;
import com.babl.citybank.modules.master.resources.SdProcessFlowMasterResource;
import com.babl.citybank.modules.master.schemas.SdProcessFlowMaster;
import com.babl.citybank.modules.master.services.AuditLogService;
import com.babl.citybank.modules.master.services.SdProcessFlowMasterService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor(onConstructor = @_(@Autowired))
public class SdProcessFlowMasterController {
    private final SdProcessFlowMasterService sdProcessFlowMasterService;
    private final AuditLogService auditLogService;
    @RequestMapping(value = "sdProcessFlowMaster/add",method= RequestMethod.POST)
    public StatusResource addSdProcessFLowMaster(@RequestBody @Validated SdProcessFlowMasterDTO sdProcessFlowMasterDTO){
        boolean status=sdProcessFlowMasterService.addSdPrrocessFLowMaster(sdProcessFlowMasterDTO);
        StatusResource statusResource=new StatusResource("Successfully","");
        if(!status){
            statusResource.setStatus("Unsuccessfully");
            return statusResource;
        }
        return statusResource;
    }

    @RequestMapping(value = "sdProcessFLowMasterMaster/delete/{id}", method = RequestMethod.POST)
    public StatusResource deleteSdProcessFLowMasterMaster(@PathVariable("id") final int id) {
        boolean status = sdProcessFlowMasterService.deleteSdProcessFLowMasterMaster(id);
        StatusResource statusResource = new StatusResource("SUCESSFULL", "");
        if (status)
            return statusResource;
        statusResource.setStatus("UNSUCESSFULL");
        return statusResource;
    }
    @RequestMapping(value = "sdProcessFlowMaster/get/{id}", method = RequestMethod.GET)
    public SdProcessFlowMasterResource getSdProcessFlowMaster(@PathVariable("id") final int id) {
        return sdProcessFlowMasterService.getSdPocessFlowMasterResource(id);
    }
    @RequestMapping(value = "sdProcessFlowMaster/update/{id}", method = RequestMethod.POST)
    public StatusResource updateSdProcessFlowMaster(@RequestBody @Validated final SdProcessFlowMasterDTO sdProcessFlowMasterDTO, @PathVariable("id") final int id) {
        boolean status = sdProcessFlowMasterService.updateSdProcessFlowMaster(sdProcessFlowMasterDTO, id);
        StatusResource statusResource = new StatusResource("SUCESSFULL", "");
        if (status)
            return statusResource;
        statusResource.setStatus("UNSUCESSFULL");
        return statusResource;
    }

    @RequestMapping(value = "sdProcessFlowMaster/getAll", method = RequestMethod.GET)
    public List<SdProcessFlowMaster> getAllSdDepartmentMaster(){
        return sdProcessFlowMasterService.getAllSdProcessFlowMaster();
    }
}
