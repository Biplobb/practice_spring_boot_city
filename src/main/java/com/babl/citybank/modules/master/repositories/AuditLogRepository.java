package com.babl.citybank.modules.master.repositories;


import com.babl.citybank.modules.master.schemas.AuditLog;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;

public interface AuditLogRepository extends JpaRepository<AuditLog, Integer> {
    List<AuditLog> findByObjectTypeAndEventTimeBetween(String tableName, LocalDateTime start,
                                                       LocalDateTime end);

}
