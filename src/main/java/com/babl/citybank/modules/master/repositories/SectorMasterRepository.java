package com.babl.citybank.modules.master.repositories;

import com.babl.citybank.modules.master.schemas.SectorMaster;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SectorMasterRepository extends JpaRepository<SectorMaster, Integer> {
}