package com.babl.citybank.modules.master.repositories;

import com.babl.citybank.modules.master.schemas.StateMaster;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StateMasterRepository extends JpaRepository<StateMaster, Integer> {
}