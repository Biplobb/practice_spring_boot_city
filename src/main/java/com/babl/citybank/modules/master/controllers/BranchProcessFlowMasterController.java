package com.babl.citybank.modules.master.controllers;

import com.babl.citybank.common.StatusResource;
import com.babl.citybank.modules.master.dtos.BranchProcessFlowMasterDTO;
import com.babl.citybank.modules.master.resources.BranchProcessFlowMasterResource;
import com.babl.citybank.modules.master.schemas.BranchProcessFlowMaster;
import com.babl.citybank.modules.master.services.BranchProcessFlowMasterService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor(onConstructor = @_(@Autowired))
public class BranchProcessFlowMasterController {
    private final BranchProcessFlowMasterService branchProcessFlowMasterService;
    @RequestMapping(value = "processFlowMaster/add",method = RequestMethod.POST)
    public StatusResource addProcessFlowMaster(@RequestBody @Validated BranchProcessFlowMasterDTO branchProcessFlowMasterDTO){
        boolean status= branchProcessFlowMasterService.addProcessFLowMaster(branchProcessFlowMasterDTO);
        StatusResource statusResource=new StatusResource("Succesfull","");
        if(!status){
            statusResource.setStatus("Unsuccessfull");
            return statusResource;
        }
        return statusResource;

    }
    @RequestMapping(value = "getProcessFlowMaster/get/{id}",method = RequestMethod.GET)
    public BranchProcessFlowMasterResource getProcessFlowMaster(@PathVariable("id") final int id){
        return branchProcessFlowMasterService.getProcessFlowMaster(id);
    }


    @RequestMapping(value = "processFlowMaster/update/{id}",method =RequestMethod.POST)
    public StatusResource updateProcessFlowMaster(@PathVariable("id") final int id,@RequestBody @Validated BranchProcessFlowMasterDTO branchProcessFlowMasterDTO){
        boolean status= branchProcessFlowMasterService.updateProcessFlowMaster(id, branchProcessFlowMasterDTO);

        StatusResource statusResource=new StatusResource("Successfull","");

        if(!status){
            statusResource.setStatus("Unsuccessfull");
            return statusResource;

        }

        return statusResource;

    }

    @RequestMapping(value = "processFlowMaster/getAll", method = RequestMethod.GET)
    public List<BranchProcessFlowMaster> getAllOccupationMaster() {
        return branchProcessFlowMasterService.getAllProcessFlowMaster();
    }


    @RequestMapping(value = "processFlow/getNextUser", method = RequestMethod.GET)
    public List<BranchProcessFlowMaster> getNextUser(@RequestParam("currentUserId") final String currentUserId,
                                                     @RequestParam("nextGlobalGroup") final String nextGlobalGroup){
        return branchProcessFlowMasterService.getNextUserInProcessFlow(currentUserId, nextGlobalGroup);
    }


}
