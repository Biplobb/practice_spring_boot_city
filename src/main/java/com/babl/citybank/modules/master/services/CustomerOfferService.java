package com.babl.citybank.modules.master.services;

import com.babl.citybank.modules.master.dtos.CustomerOfferDTO;
import com.babl.citybank.modules.master.repositories.CustomerOfferRepository;
import com.babl.citybank.modules.master.repositories.CustomerOfferTemporaryRepository;
import com.babl.citybank.modules.master.repositories.ProductMasterRepository;
import com.babl.citybank.modules.master.resources.CustomerOfferResource;
import com.babl.citybank.modules.master.schemas.CustomerOffer;
import com.babl.citybank.modules.master.schemas.CustomerOfferTemporary;
import com.babl.citybank.modules.master.schemas.ProductMaster;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;


@Slf4j
@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @_(@Autowired))
public class CustomerOfferService {
    private final EntityManager entityManager;
    private final CustomerOfferRepository customerOfferRepository;
    private final ProductMasterRepository productMasterRepository;
    private final CustomerOfferTemporaryRepository customerOfferTemporaryRepository;
    public boolean addCustomerOffer(CustomerOfferDTO customerOfferDTO){
        CustomerOffer existsCustomerOffer=customerOfferRepository.findByCustomerIdAndProductCode(customerOfferDTO.getCustomerId(),customerOfferDTO.getProductCode());
        ProductMaster existsProductMaster= productMasterRepository.findByProductCode(customerOfferDTO.getProductCode());
        if(existsCustomerOffer!=null){
            //log.info("role is already assigned to this menu");
            return false;
        }
        if(existsProductMaster!=null){
            CustomerOffer customerOffer=new CustomerOffer();
            customerOffer.setCustomerId(customerOfferDTO.getCustomerId());
            customerOffer.setProductCode(customerOfferDTO.getProductCode());
            customerOfferRepository.save(customerOffer);
            //log.info("Successfully Added");
            return true;
        }
        else{
            return false;
        }

    }
    public boolean addCustomerOffers(String customerId,String productCode){
        CustomerOffer existsCustomerOffer=customerOfferRepository.findByCustomerIdAndProductCode(customerId,productCode);
        ProductMaster existsProductMaster= productMasterRepository.findByProductCode(productCode);
        if(existsCustomerOffer!=null){
            //log.info("role is already assigned to this menu");
            return false;
        }
        if(existsProductMaster!=null){
            CustomerOffer customerOffer=new CustomerOffer();
            customerOffer.setCustomerId(customerId);
            customerOffer.setProductCode(productCode);
            customerOfferRepository.save(customerOffer);
            //log.info("Successfully Added");
            return true;
        }
        else{
            return false;
        }

    }
    public boolean deleteCustomerOffer(int id){
        CustomerOffer customerOffer=customerOfferRepository.getOne(id);

        if(customerOffer==null){
            //log.info("This id is not found");
            return false;
        }
        customerOfferRepository.delete(customerOffer);
        //log.info("Successfully Deleted");
        return true;
    }
    public CustomerOfferResource getCustomerOffer(int id){
        CustomerOffer customerOffer=customerOfferRepository.getOne(id);
        CustomerOfferResource customerOfferResource=new CustomerOfferResource();
        if(customerOffer==null){
            //log.info("This id is not found");
            return null;

        }
        customerOfferResource.setCustomerId(customerOffer.getCustomerId());
        customerOfferResource.setProductCode(customerOffer.getProductCode());
        return customerOfferResource;
    }
    public boolean updateCustomerOffer(int id,CustomerOfferDTO customerOfferDTO){
        CustomerOffer customerOffer=customerOfferRepository.findById(id);

        if(customerOffer==null){
            log.info("This "+ id +" is not found");
            return false;

        }

         customerOffer=customerOfferRepository.findByCustomerIdAndProductCode(customerOfferDTO.getCustomerId(),customerOfferDTO.getProductCode());
        ProductMaster productMaster=productMasterRepository.findByProductCode(customerOffer.getProductCode());

        if(customerOffer!=null){
            log.info("This Product code " + customerOfferDTO.getProductCode() + " is not found");
            return false;

        }
        else if(productMaster!=null){
            customerOffer=customerOfferRepository.findById(id);
            customerOffer.setId(customerOffer.getId());
            customerOffer.setCustomerId(customerOfferDTO.getCustomerId());
            customerOffer.setProductCode(customerOfferDTO.getProductCode());
            customerOfferRepository.save(customerOffer);
            return true;

        }

        return false;

    }
    public List<CustomerOffer> getAllCustomerOffer(){
        return customerOfferRepository.findAll();
    }

    public void restrictedData(List<CustomerOfferTemporary> customerOfferTemporaryList){


        customerOfferTemporaryRepository.saveAll(customerOfferTemporaryList);

        String mappingQuery = "insert into customer_offer (customer_id, product_code)" +
                " SELECT  c.customer_id, c.product_code" +
                " FROM product_master p inner join customer_offer_temporary c" +
                " on p.product_code=c.product_code where curdate() between p.effective_from and p.valid_to";



        try {
            Query query = entityManager.createNativeQuery(mappingQuery);
            query.executeUpdate();
        }
        catch (Exception e){
            System.out.println(e);
        }


    }

}
