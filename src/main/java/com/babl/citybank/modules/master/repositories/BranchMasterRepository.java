package com.babl.citybank.modules.master.repositories;


import com.babl.citybank.modules.master.schemas.BranchMaster;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BranchMasterRepository extends JpaRepository<BranchMaster, Integer> {
    BranchMaster findById(int id);
}