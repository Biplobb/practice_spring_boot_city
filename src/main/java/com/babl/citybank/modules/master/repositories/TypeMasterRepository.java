package com.babl.citybank.modules.master.repositories;

import com.babl.citybank.modules.master.schemas.TypeMaster;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TypeMasterRepository extends JpaRepository<TypeMaster, Integer> {
}