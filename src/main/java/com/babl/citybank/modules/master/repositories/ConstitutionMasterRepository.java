package com.babl.citybank.modules.master.repositories;

import com.babl.citybank.modules.master.schemas.ConstitutionMaster;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ConstitutionMasterRepository extends JpaRepository<ConstitutionMaster, Integer> {
}