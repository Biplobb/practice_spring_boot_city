package com.babl.citybank.modules.master.schemas;

import com.babl.citybank.common.SDGlobalGroupName;
import com.babl.citybank.common.SDRegion;
import com.babl.citybank.common.Status;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@Entity
@Data
@EqualsAndHashCode(callSuper = false)
public class SdProcessFlowMaster extends BaseAuditingEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private SDGlobalGroupName sdGlobalGroupName;
    private String userId;
    private String sdLocalGroupId;
    private SDRegion region;
    @ManyToOne
    @JoinColumn(name = "department_id", referencedColumnName = "id")
    private SdDepartmentMaster sdDepartmentMaster;

    @Column(name = "status", columnDefinition = "ENUM('ACTIVE', 'INACTIVE')")
    @Enumerated(EnumType.STRING)
    private Status status;


}
