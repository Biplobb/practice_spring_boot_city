package com.babl.citybank.modules.master.repositories;

import com.babl.citybank.modules.master.schemas.BandMaster;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BandMasterRepository extends JpaRepository<BandMaster, Integer> {
}