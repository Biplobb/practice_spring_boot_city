package com.babl.citybank.modules.master.dtos;

import com.babl.citybank.common.BranchGlobalGroupName;
import com.babl.citybank.common.SDRegion;
import com.babl.citybank.common.Status;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class BranchProcessFlowMasterDTO {
    private BranchGlobalGroupName globalGroupName;
    private String userUid;
    private String localGroupId;

    @NotNull
    private int branchId;
    private SDRegion region;

    private Status status;
}
