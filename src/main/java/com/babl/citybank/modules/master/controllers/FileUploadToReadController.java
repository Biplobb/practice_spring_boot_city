package com.babl.citybank.modules.master.controllers;

import com.babl.citybank.common.StatusResource;
import com.babl.citybank.modules.master.dtos.FileUploadToReadDTO;
import com.babl.citybank.modules.master.resources.CustomerOfferResource;
import com.babl.citybank.modules.master.schemas.CustomerOfferTemporary;
import com.babl.citybank.modules.master.services.FileUploadToReadService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor(onConstructor = @_(@Autowired))
public class FileUploadToReadController {
    private  final FileUploadToReadService fileUploadToReadService;
    @RequestMapping(value = "fileUpload/add",method= RequestMethod.POST)
    public void fileUpload(@RequestParam("file") final MultipartFile multipartFile){

        fileUploadToReadService.getMultipartFile(multipartFile);



    }



}
