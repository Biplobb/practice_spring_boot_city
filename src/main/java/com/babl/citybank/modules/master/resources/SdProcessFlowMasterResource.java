package com.babl.citybank.modules.master.resources;

import com.babl.citybank.common.SDGlobalGroupName;
import com.babl.citybank.common.SDRegion;
import com.babl.citybank.common.Status;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class SdProcessFlowMasterResource {
    private int id;
    private SDGlobalGroupName sdGlobalGroupName;
    private String userUid;
    private String sdLocalGroupId;
    private int departmentId;
    private SDRegion region;
    private Status status;
    private String createdBy;

    private LocalDateTime createdDate;

    private String modifiedBy;

    private LocalDateTime modifiedDate;
}
