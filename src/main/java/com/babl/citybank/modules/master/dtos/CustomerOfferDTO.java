package com.babl.citybank.modules.master.dtos;

import lombok.Data;


@Data
public class CustomerOfferDTO {
    private String customerId;
    private String productCode;
}
