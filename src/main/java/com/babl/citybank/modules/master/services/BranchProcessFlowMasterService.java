package com.babl.citybank.modules.master.services;

import com.babl.citybank.modules.master.dtos.BranchProcessFlowMasterDTO;
import com.babl.citybank.modules.master.repositories.BranchMasterRepository;
import com.babl.citybank.modules.master.repositories.BranchProcessFlowMasterRepository;
import com.babl.citybank.modules.master.resources.BranchProcessFlowMasterResource;
import com.babl.citybank.modules.master.schemas.BranchMaster;
import com.babl.citybank.modules.master.schemas.BranchProcessFlowMaster;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @_(@Autowired))
public class BranchProcessFlowMasterService {
    private final BranchProcessFlowMasterRepository branchProcessFlowMasterRepository;
    private final BranchMasterRepository branchMasterRepository;

    public boolean addProcessFLowMaster(BranchProcessFlowMasterDTO branchProcessFlowMasterDTO) {

        BranchProcessFlowMaster existBranchProcessFlowMaster = branchProcessFlowMasterRepository.findByUserId(branchProcessFlowMasterDTO.getUserUid());
        if (existBranchProcessFlowMaster != null)
            return false;


        BranchProcessFlowMaster branchProcessFlowMaster = new BranchProcessFlowMaster();
        branchProcessFlowMaster.setGlobalGroupName(branchProcessFlowMasterDTO.getGlobalGroupName());
        branchProcessFlowMaster.setUserId(branchProcessFlowMasterDTO.getUserUid());
        BranchMaster branchMaster = branchMasterRepository.findById(branchProcessFlowMasterDTO.getBranchId());

        branchProcessFlowMaster.setBranchMaster(branchMaster);
        branchProcessFlowMaster.setLocalGroupId(branchProcessFlowMasterDTO.getLocalGroupId());
        branchProcessFlowMaster.setRegion(branchProcessFlowMasterDTO.getRegion());
        branchProcessFlowMaster.setStatus(branchProcessFlowMasterDTO.getStatus());

        branchProcessFlowMasterRepository.save(branchProcessFlowMaster);
        return true;


    }

    public BranchProcessFlowMasterResource getProcessFlowMaster(int id) {
        BranchProcessFlowMaster branchProcessFlowMaster = branchProcessFlowMasterRepository.getOne(id);
        BranchProcessFlowMasterResource branchProcessFlowMasterResource = new BranchProcessFlowMasterResource();
        if (branchProcessFlowMaster == null) {
            //log.info("This id is not found");
            return null;

        }
        branchProcessFlowMasterResource.setGlobalGroupName(branchProcessFlowMaster.getGlobalGroupName());
        branchProcessFlowMasterResource.setUserUid(branchProcessFlowMaster.getUserId());

        branchProcessFlowMasterResource.setLocalGroupId(branchProcessFlowMaster.getLocalGroupId());
        branchProcessFlowMasterResource.setRegion(branchProcessFlowMaster.getRegion());
        branchProcessFlowMasterResource.setStatus(branchProcessFlowMaster.getStatus());
        return branchProcessFlowMasterResource;
    }

    public boolean updateProcessFlowMaster(int id, BranchProcessFlowMasterDTO branchProcessFlowMasterDTO) {
        BranchProcessFlowMaster branchProcessFlowMaster = branchProcessFlowMasterRepository.getOne(id);

        if (branchProcessFlowMaster != null) {

            return false;

        }


        branchProcessFlowMaster.setGlobalGroupName(branchProcessFlowMasterDTO.getGlobalGroupName());
        branchProcessFlowMaster.setUserId(branchProcessFlowMasterDTO.getUserUid());
        BranchMaster branchMaster = branchMasterRepository.findById(branchProcessFlowMasterDTO.getBranchId());

        branchProcessFlowMaster.setBranchMaster(branchMaster);
        branchProcessFlowMaster.setLocalGroupId(branchProcessFlowMasterDTO.getLocalGroupId());
        branchProcessFlowMaster.setRegion(branchProcessFlowMasterDTO.getRegion());
        branchProcessFlowMaster.setStatus(branchProcessFlowMasterDTO.getStatus());

        return true;


    }

    public List<BranchProcessFlowMaster> getAllProcessFlowMaster() {
        return branchProcessFlowMasterRepository.findAll();
    }


    public List<BranchProcessFlowMaster> getNextUserInProcessFlow(String currentUserId, String nextGlobalGroupName){
        BranchProcessFlowMaster branchProcessFlowMaster = branchProcessFlowMasterRepository.findByUserId(currentUserId);

        return branchProcessFlowMasterRepository.findByBranchMasterAndGlobalGroupName(branchProcessFlowMaster.getBranchMaster(), nextGlobalGroupName);
    }
}
