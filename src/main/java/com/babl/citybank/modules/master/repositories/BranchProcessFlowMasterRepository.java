package com.babl.citybank.modules.master.repositories;

import com.babl.citybank.modules.master.schemas.BranchMaster;
import com.babl.citybank.modules.master.schemas.BranchProcessFlowMaster;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BranchProcessFlowMasterRepository extends JpaRepository<BranchProcessFlowMaster,Integer> {
    BranchProcessFlowMaster findByUserId(String userId);
    List<BranchProcessFlowMaster> findByBranchMasterAndGlobalGroupName(BranchMaster branchMaster, String globalGroupName);

}
