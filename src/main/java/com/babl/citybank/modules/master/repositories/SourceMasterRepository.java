package com.babl.citybank.modules.master.repositories;

import com.babl.citybank.modules.master.schemas.SourceMaster;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SourceMasterRepository extends JpaRepository<SourceMaster, Integer> {
}