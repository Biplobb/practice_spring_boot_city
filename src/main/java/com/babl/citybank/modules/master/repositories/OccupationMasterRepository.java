package com.babl.citybank.modules.master.repositories;

import com.babl.citybank.modules.master.schemas.OccupationMaster;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OccupationMasterRepository extends JpaRepository<OccupationMaster, Integer> {
}