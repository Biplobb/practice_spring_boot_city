package com.babl.citybank.modules.master.repositories;

import com.babl.citybank.modules.master.schemas.BusinessDivisionMaster;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BusinessDivisionMasterRepository extends JpaRepository<BusinessDivisionMaster, Integer> {
}