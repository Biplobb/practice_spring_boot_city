package com.babl.citybank.modules.master.repositories;

import com.babl.citybank.modules.master.schemas.SdDepartmentMaster;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SdDepartmentMasterRepository extends JpaRepository<SdDepartmentMaster,Integer> {
SdDepartmentMaster findById(int id);
}
