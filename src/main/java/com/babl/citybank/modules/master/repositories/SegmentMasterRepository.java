package com.babl.citybank.modules.master.repositories;

import com.babl.citybank.modules.master.schemas.SegmentMaster;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SegmentMasterRepository extends JpaRepository<SegmentMaster, Integer> {
}