package com.babl.citybank.modules.master.repositories;

import com.babl.citybank.modules.master.schemas.SdProcessFlowMaster;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SdProcessFlowMasterRepository extends JpaRepository<SdProcessFlowMaster,Integer> {
SdProcessFlowMaster findById(int id);

}
