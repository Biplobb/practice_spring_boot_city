package com.babl.citybank.modules.master.controllers;

import com.babl.citybank.modules.master.dtos.GetAuditLogDTO;
import com.babl.citybank.modules.master.resources.AuditLogResource;
import com.babl.citybank.modules.master.services.AuditLogService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class AuditLogController {
    private final AuditLogService auditLogService;


    @RequestMapping(value = "/auditLog/get", method = RequestMethod.GET)
    public List<AuditLogResource> getAuditLog(){
        GetAuditLogDTO getAuditLogDTO = new GetAuditLogDTO("band_master", LocalDateTime.now().minusMonths(3), LocalDateTime.now());
        return auditLogService.getAuditLog(getAuditLogDTO);
    }
}
