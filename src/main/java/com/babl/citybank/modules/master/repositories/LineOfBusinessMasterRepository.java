package com.babl.citybank.modules.master.repositories;

import com.babl.citybank.modules.master.schemas.LineOfBusinessMaster;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LineOfBusinessMasterRepository extends JpaRepository<LineOfBusinessMaster, Integer> {
}