package com.babl.citybank.modules.master.repositories;

import com.babl.citybank.modules.master.schemas.GenderMaster;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GenderMasterRepository extends JpaRepository<GenderMaster, Integer> {
}