package com.babl.citybank.modules.master.resources;

import com.babl.citybank.common.BranchGlobalGroupName;
import com.babl.citybank.common.SDRegion;
import com.babl.citybank.common.Status;
import lombok.Data;

@Data
public class BranchProcessFlowMasterResource {
    private int id;
    private BranchGlobalGroupName globalGroupName;
    private String userUid;
    private String localGroupId;
    private int branchId;
    private SDRegion region;
    private Status status;

}
