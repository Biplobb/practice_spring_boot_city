package com.babl.citybank.modules.master.schemas;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@Entity(name="CustomerOfferTemporary")
public class CustomerOfferTemporary {
   @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String customerId;
    private String productCode;
}
