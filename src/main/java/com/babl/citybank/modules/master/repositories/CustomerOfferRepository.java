package com.babl.citybank.modules.master.repositories;

import com.babl.citybank.modules.master.schemas.CustomerOffer;
import org.apache.catalina.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface CustomerOfferRepository extends JpaRepository<CustomerOffer,Integer> {

    CustomerOffer findById(int id);

    CustomerOffer findByCustomerIdAndProductCode(String customerId, String productCode);
   /* @Query("SELECT C.customerId,C.productCode from ProductMaster P inner join CustomerOffer C on P.productCode=C.productCode where C.productCode=?1")
    CustomerOffer cutomerProductMappintTable(int productCode);*/
}
