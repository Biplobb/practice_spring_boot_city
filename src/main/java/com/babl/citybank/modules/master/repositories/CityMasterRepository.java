package com.babl.citybank.modules.master.repositories;

import com.babl.citybank.modules.master.schemas.CityMaster;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CityMasterRepository extends JpaRepository<CityMaster, Integer> {
}