package com.babl.citybank.modules.master.resources;

import com.babl.citybank.common.Status;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class BranchMasterResource {
    private int id;

    private String branchName;

    private String solId;

    private Status status;

    private String createdBy;

    private LocalDateTime createdDate;

    private String modifiedBy;

    private LocalDateTime modifiedDate;

}