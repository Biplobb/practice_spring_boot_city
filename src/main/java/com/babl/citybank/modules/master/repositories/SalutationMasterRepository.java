package com.babl.citybank.modules.master.repositories;

import com.babl.citybank.modules.master.schemas.SalutationMaster;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SalutationMasterRepository extends JpaRepository<SalutationMaster, Integer> {
}