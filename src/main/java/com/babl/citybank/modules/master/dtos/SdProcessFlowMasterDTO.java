package com.babl.citybank.modules.master.dtos;

import com.babl.citybank.common.SDGlobalGroupName;
import com.babl.citybank.common.SDRegion;
import com.babl.citybank.common.Status;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class SdProcessFlowMasterDTO {
 private int id;
   private SDGlobalGroupName sdGlobalGroupName;
    private String userUid;
    private String sdLocalGroupId;

    @NotNull
    private int departmentId;
    private SDRegion region;

    private Status status;
}
