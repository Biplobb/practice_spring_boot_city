package com.babl.citybank.modules.master.dtos;

import com.babl.citybank.common.Status;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class OccupationMasterAddUpdateDTO {
    @NotEmpty
    private String occupationName;

    private Status status;

}