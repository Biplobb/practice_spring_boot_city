package com.babl.citybank.modules.master.repositories;

import com.babl.citybank.modules.master.schemas.CustomerOfferTemporary;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerOfferTemporaryRepository extends JpaRepository<CustomerOfferTemporary,Integer> {
}
