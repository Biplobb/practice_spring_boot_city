package com.babl.citybank.modules.master.resources;

import lombok.Data;

@Data
public class FileUploadToReadResource {
    private String fileName;
}
