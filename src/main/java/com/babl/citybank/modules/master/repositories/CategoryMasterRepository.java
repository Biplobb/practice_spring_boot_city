package com.babl.citybank.modules.master.repositories;

import com.babl.citybank.modules.master.schemas.CategoryMaster;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryMasterRepository extends JpaRepository<CategoryMaster, Integer> {
}