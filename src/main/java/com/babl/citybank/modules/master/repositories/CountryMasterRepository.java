package com.babl.citybank.modules.master.repositories;

import com.babl.citybank.modules.master.schemas.CountryMaster;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CountryMasterRepository extends JpaRepository<CountryMaster, Integer> {
}