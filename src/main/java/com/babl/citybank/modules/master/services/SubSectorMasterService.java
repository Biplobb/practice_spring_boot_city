package com.babl.citybank.modules.master.services;

import com.babl.citybank.common.EventType;
import com.babl.citybank.modules.master.dtos.SubSectorMasterAddUpdateDTO;
import com.babl.citybank.modules.master.repositories.SubSectorMasterRepository;
import com.babl.citybank.modules.master.resources.SubSectorMasterResource;
import com.babl.citybank.modules.master.schemas.SubSectorMaster;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class SubSectorMasterService {
    private final SubSectorMasterRepository subSectorMasterRepository;
    private final AuditLogService auditLogService;

    public boolean addSubSectorMaster(SubSectorMasterAddUpdateDTO subSectorMasterAddUpdateDTO) {
        SubSectorMaster subSectorMaster = new SubSectorMaster();
        subSectorMaster.setSubSectorCode(subSectorMasterAddUpdateDTO.getSubSectorCode());
        subSectorMaster.setSubSectorName(subSectorMasterAddUpdateDTO.getSubSectorName());
        subSectorMaster.setStatus(subSectorMasterAddUpdateDTO.getStatus());
        subSectorMasterRepository.save(subSectorMaster);
        log.info("SubSectorMaster added sucessfully");

        Map<String, Object> mapData = new HashMap<>();
        mapData.put("sub_sector_name", subSectorMaster.getSubSectorName());
        mapData.put("sub_sector_code", subSectorMaster.getSubSectorCode());
        mapData.put("status", subSectorMaster.getStatus());

        auditLogService.addAuditLog(subSectorMaster, "sub_sector_master", subSectorMaster.getId(), EventType.INSERT.toString(), mapData.toString());


        return true;
    }

    public boolean updateSubSectorMaster(SubSectorMasterAddUpdateDTO subSectorMasterAddUpdateDTO, int id) {
        SubSectorMaster subSectorMaster = subSectorMasterRepository.getOne(id);
        if (subSectorMaster == null) {
            log.info("SubSectorMaster not found with id = " + id);
            return false;
        }
        subSectorMaster.setSubSectorCode(subSectorMasterAddUpdateDTO.getSubSectorCode());
        subSectorMaster.setSubSectorName(subSectorMasterAddUpdateDTO.getSubSectorName());
        subSectorMaster.setStatus(subSectorMasterAddUpdateDTO.getStatus());
        subSectorMasterRepository.save(subSectorMaster);
        log.info("SubSectorMaster updated sucessfully with id = " + id);

        Map<String, Object> mapData = new HashMap<>();
        mapData.put("sub_sector_name", subSectorMaster.getSubSectorName());
        mapData.put("sub_sector_code", subSectorMaster.getSubSectorCode());
        mapData.put("status", subSectorMaster.getStatus());

        auditLogService.addAuditLog(subSectorMaster, "sub_sector_master", subSectorMaster.getId(), EventType.UPDATE.toString(), mapData.toString());


        return true;
    }

    public boolean deleteSubSectorMaster(int id) {
        SubSectorMaster subSectorMaster = subSectorMasterRepository.getOne(id);
        if (subSectorMaster == null) {
            log.info("SubSectorMaster not found with id = " + id);
            return false;
        }
        subSectorMasterRepository.delete(subSectorMaster);
        log.info("SubSectorMaster deleted sucessfully with id = " + id);
        return true;
    }

    public SubSectorMasterResource getSubSectorMasterResource(int id){
        SubSectorMaster subSectorMaster = subSectorMasterRepository.getOne(id);
        if (subSectorMaster == null){
            log.info("SubSectorMaster not found with id = " + id);
            return null;
        }
        SubSectorMasterResource subSectorMasterResource = new SubSectorMasterResource();
        subSectorMasterResource.setId(subSectorMaster.getId());
        subSectorMasterResource.setSubSectorCode(subSectorMaster.getSubSectorCode());
        subSectorMasterResource.setSubSectorName(subSectorMaster.getSubSectorName());
        subSectorMasterResource.setStatus(subSectorMaster.getStatus());
        subSectorMasterResource.setCreatedBy(subSectorMaster.getCreatedBy());
        subSectorMasterResource.setCreatedDate(subSectorMaster.getCreatedDate());
        subSectorMasterResource.setModifiedBy(subSectorMaster.getModifiedBy());
        subSectorMasterResource.setModifiedDate(subSectorMaster.getModifiedDate());
        log.info("SubSectorMaster returned sucessfully with id = " + id);
        return subSectorMasterResource;
    }

    public List<SubSectorMaster> getAllSubSectorMaster() {
        return subSectorMasterRepository.findAll();
    }

}