package com.babl.citybank.modules.master.services;

import com.babl.citybank.common.EventType;
import com.babl.citybank.modules.master.dtos.SdProcessFlowMasterDTO;
import com.babl.citybank.modules.master.repositories.SdDepartmentMasterRepository;
import com.babl.citybank.modules.master.repositories.SdProcessFlowMasterRepository;
import com.babl.citybank.modules.master.resources.SdProcessFlowMasterResource;
import com.babl.citybank.modules.master.schemas.SdDepartmentMaster;
import com.babl.citybank.modules.master.schemas.SdProcessFlowMaster;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Transactional
@Service
@RequiredArgsConstructor(onConstructor = @_(@Autowired))
public class SdProcessFlowMasterService {
    private final SdProcessFlowMasterRepository sdProcessFlowMasterRepository;
    private final SdDepartmentMasterRepository sdDepartmentMasterRepository;
    private final AuditLogService  auditLogService;
    public boolean addSdPrrocessFLowMaster(SdProcessFlowMasterDTO sdProcessFlowMasterDTO){
        SdProcessFlowMaster sdProcessFlowMaster=new SdProcessFlowMaster();
        sdProcessFlowMaster.setSdGlobalGroupName(sdProcessFlowMasterDTO.getSdGlobalGroupName());
        sdProcessFlowMaster.setSdLocalGroupId(sdProcessFlowMasterDTO.getSdLocalGroupId());
        SdDepartmentMaster sdDepartmentMaster = sdDepartmentMasterRepository.findById(sdProcessFlowMasterDTO.getDepartmentId());
        sdProcessFlowMaster.setSdDepartmentMaster(sdDepartmentMaster);
        sdProcessFlowMaster.setRegion(sdProcessFlowMasterDTO.getRegion());
        sdProcessFlowMaster.setUserId(sdProcessFlowMasterDTO.getUserUid());
        sdProcessFlowMaster.setStatus(sdProcessFlowMasterDTO.getStatus());
        sdProcessFlowMasterRepository.save(sdProcessFlowMaster);
        Map<String,Object> mapData=new HashMap<>();
        mapData.put("sd_global_group_name",sdProcessFlowMasterDTO.getSdGlobalGroupName());
        mapData.put("sd_local_group_id",sdProcessFlowMasterDTO.getSdLocalGroupId());
        mapData.put("user_uid",sdProcessFlowMasterDTO.getUserUid());
        mapData.put("region",sdProcessFlowMasterDTO.getRegion());
        mapData.put("status",sdProcessFlowMasterDTO.getStatus());


        auditLogService.addAuditLog(sdProcessFlowMaster,"sd_process_flow_master",sdProcessFlowMasterDTO.getId(), EventType.INSERT.toString(),mapData.toString());
        return true;
    }





    public SdProcessFlowMasterResource getSdPocessFlowMasterResource(int id){
        SdProcessFlowMaster sdProcessFlowMaster = sdProcessFlowMasterRepository.getOne(id);
        if (sdProcessFlowMaster == null){
            log.info("Sd process flow master  not found with id = " + id);
            return null;
        }
        SdProcessFlowMasterResource sdProcessFlowMasterResource = new SdProcessFlowMasterResource();
        sdProcessFlowMasterResource.setId(sdProcessFlowMaster.getId());
        sdProcessFlowMasterResource.setSdGlobalGroupName(sdProcessFlowMaster.getSdGlobalGroupName());
        sdProcessFlowMasterResource.setSdLocalGroupId(sdProcessFlowMaster.getSdLocalGroupId());
        sdProcessFlowMasterResource.setRegion(sdProcessFlowMaster.getRegion());
        sdProcessFlowMasterResource.setUserUid(sdProcessFlowMaster.getUserId());

        sdProcessFlowMasterResource.setStatus(sdProcessFlowMaster.getStatus());
        sdProcessFlowMasterResource.setCreatedBy(sdProcessFlowMaster.getCreatedBy());
        sdProcessFlowMasterResource.setCreatedDate(sdProcessFlowMaster.getCreatedDate());
        sdProcessFlowMasterResource.setModifiedBy(sdProcessFlowMaster.getModifiedBy());
        sdProcessFlowMasterResource.setModifiedDate(sdProcessFlowMaster.getModifiedDate());
        log.info("SdProcessFlowMaster returned sucessfully with id = " + id);
        return sdProcessFlowMasterResource;
    }
    public boolean updateSdProcessFlowMaster(SdProcessFlowMasterDTO sdProcessFlowMasterDTO, int id) {
        SdProcessFlowMaster sdProcessFlowMaster = sdProcessFlowMasterRepository.getOne(id);
        if (sdProcessFlowMaster == null) {
            log.info("SdPocessFlowMaster not found with id = " + id);
            return false;
        }
        sdProcessFlowMaster.setSdGlobalGroupName(sdProcessFlowMasterDTO.getSdGlobalGroupName());
        sdProcessFlowMaster.setSdLocalGroupId(sdProcessFlowMasterDTO.getSdLocalGroupId());
        sdProcessFlowMaster.setRegion(sdProcessFlowMasterDTO.getRegion());
        sdProcessFlowMaster.setUserId(sdProcessFlowMasterDTO.getUserUid());
        sdProcessFlowMaster.setStatus(sdProcessFlowMasterDTO.getStatus());
        sdProcessFlowMasterRepository.save(sdProcessFlowMaster);

        log.info("SdPocessFlowMaster updated sucessfully with id = " + id);

        Map<String, Object> mapData = new HashMap<>();

        mapData.put("sd_global_group_name",sdProcessFlowMaster.getSdGlobalGroupName());
        mapData.put("sd_local_group_id",sdProcessFlowMaster.getSdLocalGroupId());
        mapData.put("user_uid",sdProcessFlowMaster.getUserId());
        mapData.put("region",sdProcessFlowMaster.getRegion());
        mapData.put("status", sdProcessFlowMaster.getStatus());

        auditLogService.addAuditLog(sdProcessFlowMaster, "sd_process_flow_master", sdProcessFlowMaster.getId(), EventType.UPDATE.toString(), mapData.toString());


        return true;
    }


    public boolean deleteSdProcessFLowMasterMaster(int id) {
        SdProcessFlowMaster sdProcessFlowMaster = sdProcessFlowMasterRepository.getOne(id);
        if (sdProcessFlowMaster == null) {
            log.info("SdProcessFlowMaster not found with id = " + id);
            return false;
        }
        sdProcessFlowMasterRepository.delete(sdProcessFlowMaster);
        log.info("SdProcessFlowMaster deleted sucessfully with id = " + id);
        return true;
    }

    public List<SdProcessFlowMaster> getAllSdProcessFlowMaster(){
        return sdProcessFlowMasterRepository.findAll();
    }
}
