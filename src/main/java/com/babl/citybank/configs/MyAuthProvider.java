package com.babl.citybank.configs;

import com.babl.citybank.modules.adminPanel.services.UserRoleService;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;


import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

@Slf4j
@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class MyAuthProvider implements AuthenticationProvider {
    private final HttpServletResponse httpServletResponse;
    private final UserRoleService userRoleService;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String username = authentication.getName();
        String password = authentication.getCredentials().toString();

        RestTemplate restTemplate = new RestTemplate();
        PMRequest pmRequest = new PMRequest();
        List<String> usernames = new ArrayList<>();

        pmRequest.setGrant_type("password");
        pmRequest.setScope("*");
        pmRequest.setClient_id("SOSYRQZTRSXQJTWKGAHUQWSJKRPBPIUW");
        pmRequest.setClient_secret("8001173835cb6b0c88e0a77015724755");
        //pmRequest.setClient_id("UNZDGMFFKRJYTGASLPDSYGPRLIITLEWW");
        //pmRequest.setClient_secret("5444966765ccfcb1a4eb874047028679");
        pmRequest.setUsername(username);
        pmRequest.setPassword(password);

        try {

            Object object = restTemplate.postForObject("http://192.168.10.211:8088/workflow/oauth2/token", pmRequest, Object.class);
            //Object object = restTemplate.postForObject("http://localhost/workflow/oauth2/token", pmRequest, Object.class);
            Map<String, ?> response = (Map) object;


            httpServletResponse.addHeader("access_token", (String) response.get("access_token"));
            httpServletResponse.addHeader("Access-Control-Expose-Headers", "access_token");

            if (response.containsKey("access_token")) {
                //List<String> rolesHRM = restTemplate.getForObject("http://192.168.10.211:8090/user/getRole/HRM/" + username, List.class);
                //List<String> roles360 = restTemplate.getForObject("http://192.168.10.211:8090/user/getRole/360/" + username, List.class);

                List<String> roles360 = userRoleService.getRoleOfUser(username);

                Collection<GrantedAuthority> grantedAuthorities = new ArrayList<>();
                

                for (String role : roles360){
                    GrantedAuthority grantedAuthority = new SimpleGrantedAuthority("ROLE_360_" + role);
                    grantedAuthorities.add(grantedAuthority);
                }

                log.info("added " + grantedAuthorities +" authorities for user "+username);
                return new UsernamePasswordAuthenticationToken(username, password, grantedAuthorities);
            }
            else
                return null;
        }
        catch (Exception e){
            System.out.println(e);
        }
        throw new BadCredentialsException("Credential not valid");
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}



@Data
class PMRequest{
    private String grant_type;

    private String scope;

    private String client_id;

    private String client_secret;

    private String username;

    private String password;
}