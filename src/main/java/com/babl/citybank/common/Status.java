package com.babl.citybank.common;

public enum Status {
    ACTIVE, INACTIVE
}
