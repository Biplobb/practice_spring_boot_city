package com.babl.citybank.common;

public enum EventType {
    INSERT, UPDATE, DELETE
}
