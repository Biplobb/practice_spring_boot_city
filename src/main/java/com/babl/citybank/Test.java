package com.babl.citybank;

import com.babl.citybank.modules.adminPanel.schemas.UserRole;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

public class Test {
//    private static  String toJson(Map<String, Object> hashMap){
//
//    }

    public static void main(String[] args) {

//        Field[] fields = UserRole.class.getDeclaredFields();
//        for (Field field : fields){
//            System.out.println(field.getName());
//            field.get()
//        }

        Map<String, Object> hashMap = new HashMap<>();
        hashMap.put("name", "Something");
        hashMap.put("value", 100);

        String hashToString = hashMap.toString();

        System.out.println(hashToString);

        String formatedString =  hashToString.replace("{", "");
        formatedString =  formatedString.replace("}", "");
        //System.out.println(formatedString);

        String[] keyValues = formatedString.split(",|=");

        Map<String, Object> newMap = new HashMap<>();

        for (int i=0; i<keyValues.length; i = i+2){
            newMap.put(keyValues[i].trim(), keyValues[i+1].trim());
        }

        System.out.println(newMap);
    }
}
